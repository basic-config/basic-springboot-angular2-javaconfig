package net.suby.wise.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.sql.DataSourceDefinition;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import net.suby.wise.login.dao.mybatis.UserDataMapper;

@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {
	@Autowired
	private UserDataMapper userDataMapper;
	
	@Transactional
	@Override
	public UserDetails loadUserByUsername(final String username)
		throws UsernameNotFoundException {

		HashMap<String, String> hashMap = new HashMap<>();
		hashMap.put("id", username);
		HashMap<String, String> hm = userDataMapper.findUserId(hashMap);
		
		Set<String> userRoles = new HashSet<String>();
		userRoles.add(hm.get("user_role").toString());
		List<GrantedAuthority> authorities = buildUserAuthority(userRoles);

		return buildUserForAuthentication(hm, authorities);

	}

	// Converts com.mkyong.users.model.User user to
	// org.springframework.security.core.userdetails.User
	private User buildUserForAuthentication(HashMap<String, String> hashMap, List<GrantedAuthority> authorities) {
		return new User(hashMap.get("user_id"), hashMap.get("password"), true, true, true, true, authorities);
	}

	private List<GrantedAuthority> buildUserAuthority(Set<String> userRoles) {

		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		// Build user's authorities
		for (String userRole : userRoles) {
			setAuths.add(new SimpleGrantedAuthority(userRole));
		}

		List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);

		return Result;
	}
	

}
